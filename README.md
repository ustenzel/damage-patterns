# damage-patterns

This is a program that reads aligned sequences and plots
misincorporation patterns along the read length.  It's based on code we
used back in 2007 for
[this paper](http://www.pnas.org/content/104/37/14616.full).  Since
then, it's use has been customary in a lot of ancient DNA work.

This version has been cleaned up substantially.  It now reads only the
standard BAM format, and by open-coding the customary charts, it became
a lot faster, but also less flexible. 

From early on, there was an embedded DSL to allow the specification of
custom charts, but using it is quite slow.

## Installation

`damage-patterns` uses Cabal, the standard installation mechanism for
Haskell.  It depends on the `biohazard` library and additional stuff
from Hackage.  To install, follow these steps:

* install GHC (see http://haskell.org/ghc)
  and cabal-install (see http://haskell.org/cabal),
* `cabal update` (takes a while to download the current package list),
* `git clone https://ustenzel@bitbucket.org/ustenzel/damage-patterns.git`
* `cabal install damage-patterns/` (takes even longer).

When done, on an unmodified Cabal setup, you will find the binary in
`${HOME}/cabal/bin`.  Cabal can install them in a different place,
please refer to the Cabal documentation at http://www.haskell.org/cabal/
if you need that.  Sometimes, repeated installations and
re-installations can result in a thoroughly unusable state of the Cabal
package collection.  If you get error messages that just don't make
sense anymore, please refer to
http://www.vex.net/~trebla/haskell/sicp.xhtml; among other useful
things, it tells you how to wipe a package database without causing
further destruction.

### Optional Charting Capability 

`damage-patterns` can output nice charts in png, svg, pdf or ps formats.
Charting depends on a number of Haskell libraries, and those may not
install on your setup.  In that case, `damage-patterns` can instead be
installed without plotting capability: `cabal install -f -Chart`.
Naturally, you'll only get textual tables, not plots.  The compiled
program tells you whether it knows how to plot when asked for its
version number: `damage-patterns -V`.

Charting does not currently work on GHC 8.10 or newer, because some
library dependencies have not been updated yet.

