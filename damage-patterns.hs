import Bio.Adna
import Bio.Adna.Query
import Bio.Bam
import Bio.Prelude
import Bio.TwoBit
import Control.Monad.Log
import Paths_damage_patterns ( version )
import System.Console.GetOpt
import System.Directory      ( createDirectoryIfMissing )
import System.FilePath

import qualified Bio.Streaming.Prelude  as Q
import qualified Control.Monad.Catch    as X
import qualified Data.Vector.Storable   as W

#ifdef CHART
import GraphUtil
#else
data Format = Txt
#endif


read'IO :: (Read a, MonadIO m) => String -> String -> m a
read'IO t s = case reads s of
    [(a,rest)] | all isSpace rest -> return a
    _ -> liftIO $ do hPutStrLn stderr $ "Error: could not parse " ++ show s ++ " as " ++ t
                     exitFailure

withProgress :: (MonadIO m, MonadMask m)
             => ( (Stream (Of a) m u -> Stream (Of a) m u) -> (Stream (Of b) m v -> Stream (Of b) m v) -> m r ) -> m r
withProgress k = do
    nr <- liftIO $ newIORef (0::Int)
    na <- liftIO $ newIORef (0::Int)

    let pr c = do u <- readIORef nr
                  v <- readIORef na
                  hPrintf stderr "\27[Kreading, %s reads, %s alignments.%c"
                                 (showNum u) (showNum v) c

    let count    = Q.chain (\_ -> liftIO $ do n <- readIORef na
                                              writeIORef na $! succ n)
        progress = Q.chain (\_ -> liftIO $ do n <- readIORef nr
                                              writeIORef nr $! succ n
                                              when (n .&. 0x3fff == 0) $ pr '\r')
    k count progress `finally` liftIO (pr '\n')


tryAnyway :: IO a -> IO ()
tryAnyway io = (io >> return ()) `X.catch` ignore
  where ignore :: X.SomeException -> IO () ; ignore _ = return ()

options :: [OptDescr ((Conf -> LIO a) -> Conf -> LIO a)]
options =
    [ Option "o" ["output"]     (ReqArg (\fn k c -> k $ c { opath = fn }) "FILE")
             "Set output path and prefix to FILE"
    , Option "x" ["xrange"]     (readIArg (\n k c -> k $ c { xrange = max 0 n }) "NUM")
             "Set range of X axis to [0..NUM]"
    , Option "C" ["context"]    (readIArg (\n k c -> k $ c { context = max 0 n }) "NUM")
             "Extract NUM nt context from reference"
    , Option "G" ["genome"] (ReqArg (\fn k c -> liftIO (openTwoBit fn) >>= \tbf -> k $ c { genome = Just tbf }) "FILE")
             "Genome in 2bit format to get context/ref from"
    , Option "F" ["format"] (ReqArg readFormat "FMT")
#ifdef CHART
             ("Set output format to FMT (svg,ps,pdf,png,txt)")
    , Option "y" ["yrange"] (readFArg (\n k c -> k $ c { yrange = Just $ min 1 (max 0 n) }) "NUM")
             "Set range of Y axis for substitution graphs to [0..NUM]"
    , Option "Y" ["yrange1"] (readFArg (\n k c -> k $ c { yrange1 = Just $ min 1 (max 0 n) }) "NUM")
             "Set range of Y axis for composition graphs to [0.25-NUM..0.25+NUM]"
    , Option "t" ["title"] (ReqArg (\t k c -> k $ c { titled = \s -> s ++ " (" ++ t ++ ")" }) "TEXT")
             "Set title of this dataset to TEXT"
    , Option "W" ["width"] (readIArg (\w k c -> k $ c { width = w }) "NUM")
             "Set width of graphs to NUM pixels"
    , Option "H" ["height"] (readIArg (\h k c -> k $ c { height = h }) "NUM")
             "Set height of graphs to NUM pixels"
    , Option "S" ["stats"] (NoArg do_aux_output) "Also output text files"
#else
             "Set output format to FMT (txt)"
    , Option "S" ["stats"] (NoArg id) "Ignored for compatibility"
#endif
    , Option "f" ["fractions"] (NoArg (\k c -> k $ c { fractions = True }))
             "Show numbers as common fractions"
    , Option "e" ["eval"] (ReqArg (\e k c -> parseExpr e >>= \f -> k $ c { expression = f }) "EXPR")
             "Evaluate EXPR on each alignment"
    , Option "n" ["limit"] (readIArg (\l k c -> k $ c { limit = Just l }) "NUM")
             "Limit run to NUM alignments"
    , Option "V" ["version"] (NoArg (\k c -> k $ c { action = PrintVersion }))
             "Output version number and exit"
    , Option "h" ["help","usage"] (NoArg (\k c -> k $ c { action = PrintHelp }))
             "Print this usage information and exit" ]
  where
    readIArg  f = ReqArg (\a k c -> read'IO "a whole number" a >>= \a' -> f (a'::Int) k c)
#ifdef CHART
    readFArg  f = ReqArg (\a k c -> read'IO "a floating point number" a >>= \a' -> f (a'::Double) k c)

    do_aux_output k c = k $ c { output_aux = True }

    readFormat a k c = case filter (not . isSpace) $ map toLower a of
             "pdf" -> k $ c { format = Pdf }
             "png" -> k $ c { format = Png }
             "txt" -> k $ c { format = Txt }
             "svg" -> k $ c { format = Svg }
             _     -> error $ "unrecognized format " ++ a
#else
    readFormat a k c = case filter (not . isSpace) $ map toLower a of
             "txt" -> k $ c { format = Txt }
             _     -> error $ "unrecognized format " ++ a
#endif

print_version :: IO ()
print_version = do
    pn <- getProgName
    putStrLn $ pn ++ ", version " ++ showVersion version
#ifdef CHART
        ++ "-Chart"
#else
        ++ "-NoChart"
#endif

print_help :: IO ()
print_help = do
    pn <- getProgName
    putStrLn $ usageInfo ("usage: " ++ pn ++ usage_info) options

usage_info :: String
usage_info = " [OPTIONS] [FILES]\n" ++
     "where FILES can be any combination of BAM and SAM files and OPTIONS may be"

data Action = Run | PrintVersion | PrintHelp

data Conf = Conf
    { opath :: FilePath
    , xrange :: Int
    , context :: Int
    , genome :: Maybe TwoBitFile
    , yrange :: Maybe Double
    , yrange1 :: Maybe Double
    , titled :: String -> String
    , width :: Int
    , height :: Int
    , format :: Format
    , fractions :: Bool
    , output_aux :: Bool
    , expression :: VFun [Stylish W.Vector Ratio]
    , limit :: Maybe Int
    , action :: Action }

defaultConf :: Conf
defaultConf = Conf
    { opath = "./"
    , xrange = 80
    , context = 10
    , genome = Nothing
    , yrange = Nothing
    , yrange1 = Nothing
    , titled = id
    , width = 1024
    , height = 768
#ifdef CHART
    , format = Svg
#else
    , format = Txt
#endif
    , fractions = False
    , output_aux = False
    , expression = pure []
    , limit = Nothing
    , action = Run }

show_common, show_decimal :: Ratio -> String
show_common (Ratio n 1) = show n
show_common (Ratio n d) = shows n "/" ++ show d

show_decimal (Ratio n 1) = show n
show_decimal (Ratio n d) = showGFloat (Just 3) p . (" ["++) . showGFloat (Just 3) u . (".."++) $ showGFloat (Just 3) v "]"
  where (u,p,v) = wilson 0.05 n d

to_table :: (Ratio -> String) -> (Int,Int) -> [Curve] -> String
to_table showD (xmin,xmax) curves =
    unlines . map (intercalate "\t") $
    ( [] : labels ) : [ show x : [ showD (row `atS` x) | row <- values ] | x <- [xmin..xmax] ]
  where
    labels = map s_label curves
    values = map (c_values . s_payload) curves


type OutputFn = String -> [Char] -> (Int, Int) -> [Char] -> [Curve] -> IO ()

stats_main :: OutputFn  -- output of basecompo
           -> OutputFn  -- output of others
           -> [FilePath] -> Conf -> LIO ()
stats_main output1 output2 files Conf{..} = do
    files' <- if null files then ["-"] <$ logStringLn "reading from stdin" else pure files

    let diter (Just f) ctx h = damagePatternsIter2Bit (meta_refs h) f ctx
        diter Nothing    0 _ = damagePatternsIterMD
        diter Nothing    x _ = error $ "context (" ++ shows x ") needs a 2bit file"

        earlyOut h = case hdr_sorting $ meta_hdr h of
            Coordinate  -> Q.takeWhile (isValidRefseq . b_rname . unpackBam)
            _           -> id

    more :> DmgStats{..} <- withProgress                $ \count progress ->
                            concatInputs files'         $ \hdr ->
                            evalVFun expression
                            . count
                            . diter genome context hdr xrange
                            . progress
                            . addFragType hdr
                            . maybe id Q.take limit
                            . earlyOut hdr

    liftIO . tryAnyway . createDirectoryIfMissing True $ takeDirectory opath

    liftIO $ output1 "5' base composition"
        (opath ++ "basecomp-5") (-context,xrange)
                          "Reference Base Composition Around 5' End"
                                (basecompos context basecompo5)

    liftIO $ output1 "3' base composition"
        (opath ++ "basecomp-3") (-xrange,context)
                          "Reference Base Composition Around 3' End"
                                (basecompos (xrange-1) basecompo3)
    liftIO $ output2 "5' substitutions"
        (opath ++ "substitutions-5") (0, xrange)
                          "Substitutions Seen From 5' End"
                                (substitutes 0 substs5)
    liftIO $ output2 "3' substitutions"
        (opath ++ "substitutions-3") (-xrange,0)
                          "Substitutions Seen Towards 3' End"
                                (substitutes (xrange-1) substs3)
    -- 5' conditional on 3'
    liftIO $ output2 "5' conditional substitutions"
        (opath ++ "cond-substitutions-5") (0, xrange)
                          "Conditional Substitutions Seen From 5' End"
                                (substitutes 0 substs5d3)
    -- 3' conditional on 5'
    liftIO $ output2 "3' conditional substitutions"
        (opath ++ "cond-substitutions-3") (-xrange,0)
                          "Conditional Substitutions Seen Towards 3' End"
                                (substitutes (xrange-1) substs3d5)
    liftIO $ output2 "5' CpG substitutions"
        (opath ++ "substitutionsCpG-5") (0, xrange)
                          "Substitutions At CpG Sites Seen From 5' End"
                                (substitutesCpG 0 substs5cpg)
    liftIO $ output2 "3' CpG substitutions"
        (opath ++ "substitutionsCpG-3") (-xrange,0)
                          "Substitutions At CpG Sites Seen Towards 3' End"
                                (substitutesCpG (xrange-1) substs3cpg)

    -- more :: [Stylish W.Vector Ratio]
    liftIO $ unless (null more) $
        output2 "Custom Chart" (opath ++ "custom_chart") (0, xrange)
                "Custom Chart" $ map (mapStylish $ Curve 0) more
  where
    basecompos off accs =
        Stylish "#" (Just hidden) (Just black) (Curve off (W.map (\x -> Ratio x 1) (W.convert sums))) :
        [ Stylish [showNucleotide nuc] (Just Solid) (Just style) $
                Curve off (W.zipWith Ratio (W.convert v) (W.convert sums))
        | (nuc, v) <- accs
        , style <- maybe [] pure $ lookup nuc [ (nucA,green), (nucC, blue), (nucG, black), (nucT, red) ] ]
      where
        sums = foldl1 (W.zipWith (+)) [ v | (_, v) <- accs ]

    substitutes off accs =
        [ Stylish [showNucleotide x, '\x2192', showNucleotide y] (Just style) (Just color) $
                Curve off (W.zipWith Ratio (W.convert v) (W.convert sv))
        | (x :-> y, v) <- accs, x /= y, (z, sv) <- sums, x == z
        | style <- [ dashed , dashed , dashed
                   , Solid  , Solid  , Solid
                   , Solid  , dotted , dotted
                   , dashdot, dashdot, dashdot ]
        | color <- [ (,,) int int 1, (,,) int int int, (,,) int  1   1
                   , (,,)  1  int 1, (,,) int int int, (,,)  1   0   0
                   , (,,)  0   1  0, (,,)  1  0.7 int, (,,) int  1   1
                   , (,,)  1  int 1, (,,)  1  0.7 int, (,,) int int int ] ]
      where
        sums = [ (x, foldl1 (W.zipWith (+)) [ v | (y :-> _, v) <- accs, x == y ])
               | x <- nub [ y | (y :-> _, _) <- accs ] ]

    substitutesCpG off accs =
        [ Stylish [showNucleotide x, '\x2192', showNucleotide y] (Just style) (Just color) $
                Curve off (W.zipWith Ratio (W.convert v) (W.convert sv))
        | (x :-> y, v) <- accs, x /= y, (z, sv) <- sums, x == z
        | style <- [ Solid, Solid , Solid
                   , Solid, dashed, dashed ]
        | color <- [ (,,) 1 int 1, (,,) int int int, (,,)  1  0 0
                   , (,,) 0  1  0, (,,)  1  0.7 int, (,,) int 1 1 ] ]
      where
        sums = [ (x, foldl1 (W.zipWith (+)) [ v | (y :-> _, v) <- accs, x == y ])
               | x <- nub [ y | (y :-> _, _) <- accs ] ]

    int     = 0.3
    dashed  = Dashed [3,3]
    dotted  = Dashed [1,2]
    dashdot = Dashed [3,2,1,2]


main :: IO ()
main = do
    ( opts, files, errors ) <- getOpt Permute options <$> getArgs
    unless (null errors) $ mapM_ (hPutStrLn stderr) errors >> exitFailure

    withLogging_ (LoggingConf Info Warning Error 10 True) $
                 (\kk -> foldr ($) kk opts defaultConf) $ \conf ->
        case action conf of
            Run          -> main' files conf
            PrintHelp    -> liftIO print_help
            PrintVersion -> liftIO print_version


main' :: [FilePath] -> Conf -> LIO ()
main' files conf@Conf{..} = do
    let showD = if fractions then show_common else show_decimal
        output_stats _ fn rng _t crv = writeFile (fn ++ ".txt") $ to_table showD rng crv
        stats_only = stats_main output_stats output_stats files conf

#ifdef CHART
        output axis msg fn rng t curves =
            case plot axis rng (titled t) curves of
                Nothing -> hPutStrLn stderr $ "Warning: " ++ msg ++ " not enough data to create plot"
                Just  p -> do hPutStrLn stderr $ "Writing " ++ msg ++ "..."
                              renderToFile fn format width height p

        output1 = output (maybe myAutoAxis theOtherAxis yrange1)
        output2 = output (maybe myAutoAxis theAxis      yrange )
        outputAux | output_aux = \k msg fn rng t cs -> k msg fn rng t cs >> output_stats msg fn rng t cs
                  | otherwise  = id

    case format of Txt -> stats_only
                   _   -> stats_main (outputAux output1) (outputAux output2) files conf
#else
    stats_only
#endif


