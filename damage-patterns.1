.TH DAMAGE-PATTERNS "1" "January 2018" "damage-patterns" "User Commands"
.SH NAME
damage-patterns \fB\-\-\fR plot or calculate substitution patterns
.SH SYNPOSIS
.BI "damage-patterns [" option ...] " file " ...

.SH DESCRIPTION
.B damage-patterns
reads BAM or SAM files containing alignments, accumulates statistics
about substitution patterns and prints them in table form or creates a
colorful plot.

Nine plots are created:  the base composition around the 5' and the 3'
end, all possible substitutions near the 5' and the 3' end, all possible
substitutions at CpG motifs near the 5' and the 3' end, and some
substitutions near the 5' and the 3' end, conditional on the presence of
C-to-T substitutions at either end.  The ninth plot is customizable.

The base composition is that of the reference.  Substitutions
use the position in the read as coordinate (in case indels make a
difference).  The values are plotted as lines and approximate 95%
confidence intervals (the Wilson Score interval is used) as shaded
regions.  Sometimes plots are skipped because of a lack of that
particular kind of data.


.SH OPTIONS

.SS Common Options

.IP "\fB\-V, --version"
Output the version number and exit.

.IP "\fB\-h, --help"
Display usage information and exit.


.SS General Options

.IP "\fB\-o, --output\fR PATH"
Sets the prefix of output files to
.IR PATH .
The default is './', i.e. output goes to the current directory and no
prefix is prepended.

.IP "\fB\-C, --context\fR NUM"
Displays the base composition for an additional
.I NUM
bases of context around either end of the aligned query.
Note that context requires that a genome file is given.

.IP "\fB\-G, --genome\fR FILE"
Declares that the reference genome is available in
.I FILE
in
.I 2bit
format.  If a genome is given, context becomes available and the input
does not need to have a valid 
.I MD
field.

.IP "\fB\-x, --xrange\fR NUM"
Sets the range of the x axis to
.IR [0..NUM] .
Plots get the appropriate scale, tables contain the appropriate lines.
The default is 80.

.IP "\fB\-f, --fractions\fR"
Causes numbers in tables to be shown as common fractions (instead of
decimal fractions) where applicable.  Common fractions allow an estimate
of confidence intervals, decimal fractions don't.

.IP "\fB--limit\fR num"
Limits the run to 
.I num
alignments.

.SS Custom Chart Options

.IP "\fB--interpret\fR expr"
Evaluates 
.I expr
as the specification for the custom chart using a rather slow
interpreter.  See
.I Expression Language
below.

.SS Graphical Only Options

The following options are only applicable if
.B damage-patterns 
was compiled with support for plotting.  A version without plotting
capability will not accept these and supports only the 'txt' output
format.

.IP "\fB\-y, --yrange\fR NUM"
Sets the range of the y axis of substitution plots to
.IR [0..NUM] .
Note that 
.I NUM
is a floating point number, not a percentage.  The default is to adjust
automatically.
 
.IP "\fB\-Y, --yrange1\fR NUM"
Sets the range of the y axis for base composition plots to
.IR [0.25-NUM..0.25+NUM] ,
that is, symmetrical around one quarter.  The default is to adjust
automatically.

.IP "\fB\-t, --title\fR TEXT"
Sets the title of this data set to
.I TEXT.
Plots will mention the data set title in their heading.

.IP "\fB\-F, --format\fR FORMAT"
Sets the output format of plots to 
.IR FORMAT .
Legal values are
.IR svg ", " ps ", " pdf ", " png " and " txt .
The
.I txt
format will not actually produce a plot, but a table with the values
that would have been plotted.  The default is 
.IR svg .

.IP "\fB\-W, --width\fR NUM"
Sets the width of plots to
.I NUM
pixels.

.IP "\fB\-H, --height\fR NUM"
Sets the height of plots to
.I NUM
pixels.

.IP "\fB\-S, --stats\fR"
Writes the text files in addition to the graphical charts.

.SS "Expression Language"

The expression argument for the
.IR --jit " and " --interpret
options specifies a list of functions to compute for one "custom chart".
Functions are created from predicates, limited arithmetic, and
attributes for charting.  For each predicate and each position along a
read, 
.B damage-patterns
counts on how many input alignments the predicate evaluates to true.
Some limited arithmetic must be performed on the totals, resulting in
a ratio of whole numbers for each position.  The ratios are charted
along with a confidence interval (the Wilson score interval).

Expressions will often contain spaces and shell meta characters, so they
should be quoted appropriately.  Primitive predicates are:

.TP
.B "A C G T N"
Match exactly the named base.  Not that N is not a wild card, it
matches Ns and only Ns.

.TP
.B X
Matches any proper base.  This is equivalent to 
.BR "A | C | G | T" .

.TP
.B gap
Matches a gap.

.TP
.B any
Matches anything, including ambiguous bases and gaps.

.TP
.BR fromL " n"
Matches if the current position is at least n from the left edge of the
alignment.

.TP
.BR fromR " n"
Matches if the current position is at least n from the right edge of the
alignment.

.PP
Valid operators, in order of decreasing precendence, are:

.TP
.BR ( \&.\|.\|. )
Grouping

.TP
.BR no
Logical negation of the following predicate.

.TP 
.B "before after"
Applies the following predicate to the next or the previous position,
respectively.  For example,
.B "C & before G"
would match on the C of a CpG motif.

.TP
.B "was is"
Applies the following predicate, which applies to a sequence, to the
reference base or the query base, respectively, turning it into a
predicate that applies to an alignment.

.TP
.B "/"
Computes the ratio of two whole numbers.  Ratios can be charted, but no further
arithmetic on ratios is supported.  The charts will not make sense unless the
resulting values are between zero and one.

.TP
.B "*"
Product of whole numbers.

.TP
.B "+ \-"
Sum and difference of whole numbers.

.TP
.B "|"
Logical or.

.TP
.B "&"
Logical and.

.TP
.B ","
Separates functions so they can be plotted in the same chart.

.PP
Styling can appear anywhere and applies to the following expression.
Valid style operators are:

.TP
.BR "#" rrggbb
Sets the line color to an arbitrary RGB value, expressed in hex notation.
For example, 
.B #FF0000
would be pure red.

.TP
.B red green blue black
Set a predefined line color.

.TP 
.B solid dashed dotted dashdot dashdotdot
Set a predefined line pattern.  Note that the abundance of line patterns
and color does not imply that a chart with many functions is still
readable.

.TP
.BR \(dq label \(dq
Sets the label for the following function.  The label can include
arbitray Unicode escapes of the form
.BR "\eu" xxxxxx
with up to six hexadecimal digits.  (Whether 
.B \eu1F4A9
"PILE OF POO" can indeed be displayed depends on which fonts are
installed on the system.)  To include a backslash or a double quote in a
label, escape them with a backslash.


.SH "FILES"

.IP BAM
is the most standard format for aligned reads, chiefly from high
throughput sequencing platforms.  If possible, use Bam as input.  See
.I http://samtools.github.io
for more information.

.B damage-patterns
will read the reference sequence from the
.I 2bit
file if given.  Else it will reconstruct the reference from the 
.I MD 
field.  Bam files with strangely miscoded 
.I MD 
fields have been spotted in the wild.  If you get errors about
inconsistent 
.I MD
fields, either fix them by running
.I samtools calmd
or simply supply a
.I 2bit
file.

.IP SAM
is effectively the text form of BAM, and is pretty much equivalent
to it, but bloated.  Use BAM if you can, but SAM is fine if and only if
it saves a needless conversion step.

.IP 2bit
is a compact format for genomes used by
.IR BLAT
and other software by UCSC.
See 
.I https://genome.ucsc.edu/goldenpath/help/blatSpec.html
for details.
.BR heffalump (1)
can also read and create 
.I 2bit
file.  (The alternative to
.I 2bit
used by
.I samtools
would be indexed
.IR FastA .
Clearly, 
.I 2bit
is superior in every conceivable way.)

.SH "BUGS"

The embedded language and its integration with the main program is not a
great design.  It should be treated as experimental.

.SH "AUTHOR"
Written by Udo Stenzel <udo_stenzel@eva.mpg.de>.

